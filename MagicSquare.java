import java.sql.Timestamp;
import java.util.ArrayList;

class State {
    private int[][] matrix;

    public State(int[][] matrix) {
        this.matrix = matrix;
    }

    public State(int size) {
        matrix = new int[size][size];
    }

    private static boolean isComplete(int[] line) {
        for (int i = 0; i < line.length; i++) {
            if (line[i] == 0) {
                return false;
            }
        }
        return true;
    }

    private int[] getColumn(int i) {
        int[] col = new int[matrix.length];
        for (int row = 0; row < col.length; row++) {
            col[row] = matrix[i][row];
        }
        return col;
    }

    private int[] getRow(int i) {
        int[] row = new int[matrix.length];
        for (int col = 0; col < row.length; col++) {
            row[col] = matrix[col][i];
        }
        return row;
    }

    private int[] getDiagonal(boolean positiveSlope) {
        int[] line = new int[matrix.length];

        for (int i = 0; i < line.length; i++) {
            int row = i;
            if (positiveSlope) {
                row = line.length - 1 - i;
            }
            line[i] = matrix[i][row];
        }

        return line;
    }

    public static int sumLine(int[] line) {
        int sum = 0;
        for (int i = 0; i < line.length; i++) {
            sum += line[i];
        }
        return sum;
    }

    public boolean isFeasible() {
        // Check all rows and columns
        for (int i = 0; i < matrix.length; i++) {
            int[] col = getColumn(i);
            if (State.isComplete(col) && State.sumLine(col) != MagicSquare.magicNumber) {
                return false;
            }

            int[] row = getRow(i);
            if (State.isComplete(row) && State.sumLine(row) != MagicSquare.magicNumber) {
                return false;
            }
        }

        // Check diagonals
        int[] positiveDiagonal = getDiagonal(true);
        int[] negativeDiagonal = getDiagonal(false);

        if (State.isComplete(positiveDiagonal) && State.sumLine(positiveDiagonal) != MagicSquare.magicNumber) {
            return false;
        }

        if (State.isComplete(negativeDiagonal) && State.sumLine(negativeDiagonal) != MagicSquare.magicNumber) {
            return false;
        }

        return true;
    }

    public boolean hasNumber(int n) {
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix.length; col++) {
                if (matrix[col][row] == n) {
                    return true;
                }
            }
        }
        return false;
    }

    public ArrayList<State> generateMoves() {

        // Find the next square to fill in
        boolean done = false;
        int chCol = 0, chRow = 0;
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix.length; col++) {
                if (matrix[col][row] == 0) {
                    done = true;
                    chCol = col;
                    chRow = row;
                    break;
                }
            }
            if (done) {
                break;
            }
        }

        ArrayList<State> moves = new ArrayList<State>();

        // For each possible value of the square, create a new state.
        for (int i = 1; i < (matrix.length * matrix.length) + 1; i++) {

            // prevent duplicate numbers in the square
            if (hasNumber(i)) {
                continue;
            }

            int[][] newMatrix = new int[matrix.length][matrix.length];
            for (int row = 0; row < matrix.length; row++) {
                for (int col = 0; col < matrix.length; col++) {
                    newMatrix[col][row] = matrix[col][row];
                }
            }
            newMatrix[chCol][chRow] = i;
            State newState = new State(newMatrix);
            moves.add(newState);
        }
        return moves;
    }

    public String toString() {
        String s = "";
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix.length; col++) {
                s += matrix[col][row] + " ";
            }
            s += "\n";
        }
        return s;
    }
}

class MagicSquare {
    public static int magicNumber = 15;

    public static void breadthFirstSearch(State initial, boolean breadthFirst) {
        int count = 1;
        int statesVisited = 0;
        ArrayList<State> frontier = new ArrayList<>();
        frontier.add(initial);
        while (frontier.size() > 0) {
            // Always removing the first element in the list
            State current = frontier.remove(0);
            if (current.isFeasible()) {
                // Expand the current state
                ArrayList<State> moves = current.generateMoves();
                if (moves.size() == 0) {
                    // It has every number and is feasible which means it is a complete solution
                    System.out.println("Solution: " + count++);
                    System.out.println(current);
                } else {
                    if (breadthFirst) {
                        // Add these possible moves to the frontier
                        frontier.addAll(moves);
                    } else {
                        // Prepend the moves for a depth-first search
                        moves.addAll(frontier);
                        frontier = moves;
                    }
                }
            }
            statesVisited++;
            if (statesVisited % 10000000 == 0) {
                System.out.println("Checked " + statesVisited / 1000000 + "M possible configurations.");
            }
        }
        if (count == 1) {
            System.out.println("No solutions found!");
        }
    }

    public static void main(String args[]) {
        if (args.length != 3) {
            System.out.println("Usage: java MagicSquare <size> <magic number> <breadth/depth first : true/false>");
            return;
        }
        int size = 3;
        boolean breadthFirst = true;
        try {
            size = Integer.parseInt(args[0]);
            MagicSquare.magicNumber = Integer.parseInt(args[1]);
            breadthFirst = Boolean.parseBoolean(args[2]);
        } catch (NumberFormatException e) {
            System.out.println("<size> and <magic number> must be integers.");
            return;
        }
        System.out.println("Finding solutions for the " + size + " x " + size + " magic square");
        System.out.println("Magic number " + magicNumber);
        System.out.println("Using " + (breadthFirst ? " breadth-first " : "depth-first") + " method");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println("Started at " + timestamp);
        State s = new State(size);
        breadthFirstSearch(s, breadthFirst);
    }
}
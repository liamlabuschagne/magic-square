# Magic Squares - 301 Lab Exercise 1
This exercise attempts to find the general solution to the magic square problem defined as finding the
valid configurations of an n*n matrix of positive integers such that each row, column and diagonal sum
to a magic number.

To do this I will use a state space (or graph) to map out all the possible configurations and then traverse this graph
to find all valid solutions, avoiding any invalid states (those which do not conform to the magic square rules). The
complete solutions will be found at the leaf nodes of this directed tree graph.

Some definitions of terminology follows:
* Feasible means that all complete rows, columns and diagonals sum to 15
* Infeasible means that at least one row, column or diagonal with all three entries assigned does
not sum to 15
* Partial means that not all entries in the array have been assigned a number
* Complete means that all entries have been assigned (i.e. the state is either feasible or infeasible)

So by this definition the goal of this program is to find all complete, feasible solutions given a size n and a magic number m as input.

## Usage
```
javac MagicSquare.java && java MagicSquare <size> <magic number> <breadth/depth first : true/false>
```